$(function() {

// Бургер меню
$(".burger-menu").click(function(){
  $(this).toggleClass("active");
  $('.main-menu').fadeToggle(200);
});



// FansyBox
 $('.fancybox').fancybox({
 	beforeShow : function(){
   this.title =  this.title + " - " + $(this.element).data("caption");
  }
 });


// Карусели
$('.projects-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});


$('.team-slider').slick({
  infinite: true,
  slidesToShow: 5,
  slidesToScroll: 1,
  arrows: true,
  responsive: [
    {
      breakpoint: 1199,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
      }
    }
  ]
});


// Фиксированное меню при прокрутке страницы вниз

$(window).scroll(function(){
    var sticky = $('.header-top'),
        scroll = $(window).scrollTop();
    if (scroll > 200) {
        sticky.addClass('header-top__fixed');
    } else {
        sticky.removeClass('header-top__fixed');
    };
});


if ($(window).width() <= '768'){
	$('.main-menu a').click(function() {
		$(this).parents('.main-menu').fadeToggle();
		$(".burger-menu").toggleClass('active')
	})
}


// Скролл по блокам
function scroll_active() {
 
        $('.scroll').each(function(){
            var window_top = $(window).scrollTop();
            var position = $(this).offset().top-95;
 
            if (window_top > position) {
                 var rell = $('a[data-rel="' + $(this).attr('data-name') + '"]');
                $(".main-menu a").removeClass('active');
                rell.addClass("active");
            }        

        });
    }


$(window).scroll(function(){
    scroll_active();
    
}); 
$(".main-menu").on("click","a", function (event) {
    //отменяем стандартную обработку нажатия по ссылке
    event.preventDefault();

    //забираем идентификатор бока с атрибута href
    var id  = $(this).attr('href'),

    //узнаем высоту от начала страницы до блока на который ссылается якорь
      top = $(id).offset().top;
    
    //анимируем переход на расстояние - top за 1500 мс
    $('body,html').animate({scrollTop: top - 90}, 1500);
  });


// BEGIN of script WOW.
new WOW().init();



//Begin of GoogleMaps
		  var myCenter=new google.maps.LatLng(56.019719, 92.890091);
		  var marker;
		  function initialize()
		  {
		    var mapProp = {
		    center:myCenter,
		    zoom:16,
		    disableDefaultUI:true,
		    zoomControl: true,
		    scrollwheel: false,
		    styles: [
			    {
			        "featureType": "all",
			        "elementType": "labels.text.fill",
			        "stylers": [
			            {
			                "saturation": 36
			            },
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 40
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.text.stroke",
			        "stylers": [
			            {
			                "visibility": "on"
			            },
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "all",
			        "elementType": "labels.icon",
			        "stylers": [
			            {
			                "visibility": "off"
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 20
			            }
			        ]
			    },
			    {
			        "featureType": "administrative",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            },
			            {
			                "weight": 1.2
			            }
			        ]
			    },
			    {
			        "featureType": "landscape",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 20
			            }
			        ]
			    },
			    {
			        "featureType": "poi",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 21
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.fill",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            }
			        ]
			    },
			    {
			        "featureType": "road.highway",
			        "elementType": "geometry.stroke",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 29
			            },
			            {
			                "weight": 0.2
			            }
			        ]
			    },
			    {
			        "featureType": "road.arterial",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 18
			            }
			        ]
			    },
			    {
			        "featureType": "road.local",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 16
			            }
			        ]
			    },
			    {
			        "featureType": "transit",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 19
			            }
			        ]
			    },
			    {
			        "featureType": "water",
			        "elementType": "geometry",
			        "stylers": [
			            {
			                "color": "#000000"
			            },
			            {
			                "lightness": 17
			            }
			        ]
			    }
			],
		    zoomControlOptions: {
		        position: google.maps.ControlPosition.RIGHT_CENTER
		    },
		    mapTypeId:google.maps.MapTypeId.ROADMAP
		    };
		      var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);
		      var marker=new google.maps.Marker({
		      position:myCenter,
		      icon: "img/marker.svg"
		      // icon: "../img/marker.png" на  хосте может так рабоать     
		      });
		    marker.setMap(map);
		    var content = document.createElement('div');
		    content.innerHTML = '<strong class="maps-caption">КВАДРАТ</strong>';
		    var infowindow = new google.maps.InfoWindow({
		     content: content
		    });
		      google.maps.event.addListener(marker,'click',function() {
		        infowindow.open(map, marker);
		        map.setCenter(marker.getPosition());
		      });
		      }
		      google.maps.event.addDomListener(window, 'load', initialize);
		//End of GoogleMaps


})